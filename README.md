## API Consumption example

# /api/product/bulk_insert

```
curl -vw "$1\n" --header "Content-Type: application/json"   --request POST   --data @data.json http://localhost:8000/api/products/bulk_insert
```

Where @data.json is your bulk data.

# /api/product

```
curl -vw "$1\n" --request GET http://localhost:8000/api/products
```