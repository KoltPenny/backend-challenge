from django.urls import path

from . import views

urlpatterns = [
    path('products/bulk_insert', views.bulk_insert, name='bulk_insert'),
    path('products', views.all_prods, name='products'),
]
