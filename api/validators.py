from django.core.exceptions import ValidationError

def validate_name(val):
    if len(val) < 3 or len(val) > 55:
        raise ValidationError("Invalid product name")

def validate_value(val):
    if (not val > 0) or (val > 99999.99):
        raise ValidationError("Invalid product value")

def validate_discount(val,disc):
    if val < disc:
        raise ValidationError("Invalid discount value")
    
def validate_stock(val):
    if not val > -1:
        raise ValidationError("Invalid stock value")

