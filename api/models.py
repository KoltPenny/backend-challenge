from django.db import models
from .validators import *

class Product(models.Model):
    id = models.CharField(max_length=200,primary_key=True)
    name = models.CharField(max_length=55)
    value = models.FloatField()
    discount_value = models.FloatField()
    stock = models.IntegerField()
        
    def clean_name(self):
        try:
            validate_name(self.name)
        except (ValidationError) as e:
            return str(e)
        return None
            
    def clean_value(self):
        try:
            validate_value(self.value)
        except (ValidationError) as e:
            return str(e)
        return None
        
    def clean_discount(self):
        try:
            validate_discount(self.value,self.discount_value)
        except (ValidationError) as e:
            return str(e)
        return None
    
    def clean_stock(self):
        try:
            validate_stock(self.stock)
        except (ValidationError) as e:
            return str(e)
        return None

    def clean(self):
        return [x.strip("'[]") for x in [
            self.clean_name(),
            self.clean_value(),
            self.clean_discount(),
            self.clean_stock()] if x is not None]

    def __str__(self):
        return str({"id":self.id,"name":self.name,"value":self.value,"discount_value":self.discount_value,"stock":self.stock})
