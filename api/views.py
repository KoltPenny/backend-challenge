from django.shortcuts import render
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from api.models import Product

import json

@csrf_exempt
def bulk_insert(request):
    if request.method == "POST":
        payload = json.loads(request.body)

        product_tuple = analyse_products(payload)

        if(product_tuple[0] == 200):
            for product in product_tuple[2]:
                print(product)
                prod = Product(
                    product['id'],
                    product['name'],
                    product['value'],
                    product['discount_value'],
                    product['stock'])

                prod.save()

            new_resp = json.dumps(product_tuple[1])
        else:
            new_resp = json.dumps({"status":"ERROR","products_report":product_tuple[1],"number_of_products_unable_to_parse":len(product_tuple[1])})

        
        response = HttpResponse( new_resp, content_type='application/json' )
        response.status_code = product_tuple[0]
        
        return response

@csrf_exempt
def all_prods(request):
    if request.method == "GET":

        query = json.dumps(  {"products":list(Product.objects.values())}  )
        response = HttpResponse(query,content_type='application/json')

        return response

def analyse_products(payload):
    
    productList = {}
    productList['elements'] = []
    productList['errors'] = []
    
    for product in payload:
        
        prod = Product(
            product['id'],
            product['name'],
            product['value'],
            product['discount_value'],
            product['stock'])
        
        errors = prod.clean()
        productList['elements'].append(product)
        
        if len(errors) != 0:
            productList['errors'].append(
                {
                    "product_id":product['id'],
                    "errors":errors
                })

    status_code = 200
    status_body = {"status":"OK"}
    
    if len(productList['errors']) > 0:
        status_code = 422
        status_body = productList['errors']

    return [status_code,status_body,productList['elements']]
